/*
 * pico8.cpp
 *
 *  Created on: Apr 29, 2016
 *      Author: hinkokocevar
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>

#include <epicsString.h>
#include <epicsMutex.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExport.h>
#include <epicsExit.h>

#include <asynNDArrayDriver.h>

#include <iocsh.h>

#include <amc_pico.h>
#include "pico8.h"


static const char *driverName = "pico8";

/**
 * Exit handler, delete the pico8 object.
 */
static void exitHandler(void *drvPvt) {
    pico8 *pPico8 = (pico8 *) drvPvt;
    delete pPico8;
}

static void dataTaskC(void *drvPvt) {
    pico8 *pPvt = (pico8 *) drvPvt;
    pPvt->dataTask();
}

/** Constructor for pico8; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method sets reasonable default values for all of the
  * parameters.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] devicePath Path to the /dev entry (usually /dev/amc_pico_<PCI slot>)
  * \param[in] numSamples The initial number of AI samples.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
pico8::pico8(const char *portName, const char *devicePath, int numSamples,
        int maxBuffers, size_t maxMemory, int priority, int stackSize)

    : asynNDArrayDriver(portName,
            PICO8_NR_CHANNELS,
            maxBuffers, maxMemory,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            ASYN_CANBLOCK | ASYN_MULTIDEVICE, /* asyn flags*/
            1,                                /* autoConnect=1 */
            priority, stackSize),
            uniqueId_(0), acquiring_(0)
{
    D(printf("driver has %d asyn addresses, %d AOI\n", maxAddr, maxAddr-PICO8_NR_CHANNELS));

    int status = asynSuccess;
    static const char *functionName = "pico8";

    mRunThread = false;
    mHandle = -1;
    snprintf(mDevicePath, PICO8_BUFFER_SIZE, "%s", devicePath);

    /* Create the epicsEvents for signaling to the simulate task when acquisition starts and stops */
    this->startEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!this->startEventId_) {
        printf("%s:%s epicsEventCreate failure for start event\n",
            driverName, functionName);
        return;
    }
    this->stopEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!this->stopEventId_) {
        printf("%s:%s epicsEventCreate failure for stop event\n",
            driverName, functionName);
        return;
    }

    /* Create an EPICS exit handler */
    epicsAtExit(exitHandler, this);

    // digitizer
    createParam(Pico8Acquire_,          asynParamInt32,       &mPico8Acquire);
    createParam(Pico8NumSamples_,       asynParamInt32,       &mPico8NumSamples);
    createParam(Pico8FSamp_,            asynParamInt32,       &mPico8FSamp);
    createParam(Pico8TickValue_,        asynParamFloat64,     &mPico8TickValue);
    createParam(Pico8ActualSamples_,    asynParamInt32,       &mPico8ActualSamples);
    createParam(Pico8TrgMode_,          asynParamInt32,       &mPico8TrgMode);
    createParam(Pico8TrgCh_,            asynParamInt32,       &mPico8TrgCh);
    createParam(Pico8TrgLevel_,       asynParamFloat64,       &mPico8TrgLevel);
    createParam(Pico8TrgLength_,        asynParamInt32,       &mPico8TrgLength);
    createParam(Pico8RingBuf_,          asynParamInt32,       &mPico8RingBuf);
    createParam(Pico8GateMux_,          asynParamInt32,       &mPico8GateMux);
    createParam(Pico8ConvMux_,          asynParamInt32,       &mPico8ConvMux);
    createParam(Pico8Message_,          asynParamOctet,       &mPico8Message);
    createParam(Pico8RateLimit_,        asynParamInt32,       &mPico8RateLimit);
    createParam(Pico8TrgLevelUnits_,    asynParamInt32,       &mPico8TrgLevelUnits);
    // individual channel
    createParam(Pico8CHEnable_,         asynParamInt32,       &mPico8CHEnable);
    createParam(Pico8CHRange_,          asynParamInt32,       &mPico8CHRange);

    /* Set some default values for parameters */
    setIntegerParam(mPico8Acquire,       0);
    setIntegerParam(mPico8NumSamples,    numSamples);
    /* pico8 firmware delivers samples as IEEE-754 floats */
    setIntegerParam(NDDataType,          NDFloat32);
    mChRangeMask = 0;

    // maximum size of raw buffer we need (user can only set lower values)
    // no need to reallocate
    mRawDataBuf = (epicsFloat32 *)calloc(1, numSamples * 4 * 8);
    mRawDataBufSize = numSamples * 4 * 8;
    D(printf("allocated %d bytes for raw data buffer\n", mRawDataBufSize));

    callParamCallbacks();

    if (stackSize == 0) {
        stackSize = epicsThreadGetStackSize(epicsThreadStackMedium);
    }

    /* Create the thread that does data readout */
    char scratch[256] = {0};
    snprintf(scratch, 255, "%s%s", "pico8", portName);

    status = (epicsThreadCreate(scratch,
            epicsThreadPriorityMedium, stackSize, (EPICSTHREADFUNC)dataTaskC, this) == NULL);
    if (status) {
        E(printf("epicsThreadCreate failure for data task\n"));
        return;
    }

    this->lock();
    if (openDevice() == 0) {
        mRunThread = true;
    }
    this->unlock();

    I(printf("init done!\n"));
}

pico8::~pico8() {
    I(printf("shutting down..\n"));

    this->lock();
    mRunThread = false;
    this->unlock();
    // this will wake up thread while waiting for acquisition to start
    setAcquire(1);
    sleep(2);
    closeDevice();

    I(printf("shutdown complete!\n"));
}

int pico8::openDevice() {
    int ret;

    ret = open(mDevicePath, O_RDWR);
    if (ret == -1) {
        E(printf("open() failed: %s\n", strerror(errno)));
        return ret;
    }
    I(printf("open() success for %s\n", mDevicePath));

    mHandle = ret;
    return 0;
}

void pico8::closeDevice() {
    if (mHandle > 0) {
        close(mHandle);
    }
    mHandle = -1;
}

int pico8::readDevice(void *buf, int samp, int *count) {
    int ret;
    int btrans;

    /* read samples of 4 bytes each for all 8 channels */
    ret = read(mHandle, buf, samp * PICO8_BLOCK_SIZE);
    if (ret == -1) {
        E(printf("read() failed: %s\n", strerror(errno)));
        return ret;
    }

    btrans = 0;
    ret = this->getBTrans(&btrans);
    if (ret == -1) {
        return ret;
    }
    *count = btrans;

    return 0;
}

int pico8::abortRead() {
    int ret;
    ret = ioctl(mHandle, ABORT_READ);
    if (ret == -1) {
        E(printf("ioctl() ABORT_READ failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setFSamp(uint32_t val) {
    int ret;
    ret = ioctl(mHandle, SET_FSAMP, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_FSAMP failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setRange(uint8_t val) {
    int ret;
    ret = ioctl(mHandle, SET_RANGE, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_RANGE failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::getBTrans(int *val) {
    int ret;
    ret = ioctl(mHandle, GET_B_TRANS, val);
    if (ret == -1) {
        E(printf("ioctl() GET_B_TRANS failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setGateMux(uint32_t val) {
    int ret;
    ret = ioctl(mHandle, SET_GATE_MUX, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_GATE_MUX failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setConvMux(uint32_t val) {
    int ret;
    ret = ioctl(mHandle, SET_CONV_MUX, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_CONV_MUX failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setRingBuf(uint32_t val) {
    int ret;
    ret = ioctl(mHandle, SET_RING_BUF, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_RING_BUF failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

int pico8::setTrigger() {
    int ret;
    struct trg_ctrl val;
    int mode;
    double level;
    int length;
    int ch;
    int units;

    getIntegerParam(mPico8TrgMode, &mode);
    getIntegerParam(mPico8TrgLength, &length);
    getIntegerParam(mPico8TrgCh, &ch);
    getDoubleParam(mPico8TrgLevel, &level);
    getIntegerParam(mPico8TrgLevelUnits, &units);
    switch (units) {
        default: // default to nano
        case 0: // nano
            level *= 1e-9;
            break;
        case 1: // micro
            level *= 1e-6;
            break;
        case 2: // mili
            level *= 1e-3;
            break;
    }
    D(printf("SET_TRG level set at %g A (units %d)\n", level, units));

    val.limit = level;
    val.nr_samp = length;
    val.ch_sel = ch;
    val.mode = (trg_ctrl::mode_t)mode;

    ret = ioctl(mHandle, SET_TRG, &val);
    if (ret == -1) {
        E(printf("ioctl() SET_TRG failed: %s\n", strerror(errno)));
        return ret;
    }

    return 0;
}

void pico8::setAcquire(int value) {
    if (value && !acquiring_) {
        /* Send an event to wake up the task */
        D(printf("waking up!\n"));
        epicsEventSignal(this->startEventId_);
    }
    if (!value && acquiring_) {
        /* This was a command to stop acquisition */
        /* Send the stop event */
        D(printf("stopping!\n"));
        abortRead();
        epicsEventSignal(this->stopEventId_);
    }
}

int pico8::acquireArrays() {
    int numSamples;
    getIntegerParam(mPico8NumSamples, &numSamples);

    // figure out how many channels are enabled
    int nrEnabledCh = 0;
    for (int a = 0; a < PICO8_NR_CHANNELS; a++) {
        if (this->pArrays[a]) {
            this->pArrays[a]->release();
        }
        this->pArrays[a] = NULL;

        int chEnable = 0;
        getIntegerParam(a, mPico8CHEnable, &chEnable);
        if (chEnable) {
            D(printf("channel enabled, addr %d\n", a));
            nrEnabledCh++;
        } else {
            D(printf("channel disabled, addr %d\n", a));
        }
    }

    // should stop the acquisition in caller
    if (nrEnabledCh == 0) {
        E(printf("no channels are enabled, aborting\n"));
        ERR_MSG("no channels are enabled, aborting");
        return -1;
    }

    /* device returns interleaved sample / channel data:
     *               channels
     * sample 1 : 0 1 2 3 4 5 6 7
     * sample 2 : 0 1 2 3 4 5 6 7
     * ...
     * we need to create linear data per channel out of this input,
     * primarily for CA clients (waveform record)
     */

    assert(numSamples <= mRawDataBufSize);
    int numBytes;

    // unlock mutex so that user can cancel the operation if
    // trigger condition has not been met (no trigger, wrong
    // configuration); otherwise IOC locks up and IOC restart
    // is required!
    this->unlock();
    int ret = readDevice(mRawDataBuf, numSamples, &numBytes);
    if (ret == -1) {
        this->lock();
        E(printf("error reading data from device, aborting\n"));
        ERR_MSG("error reading data from device, aborting");
        return ret;
    }
    this->lock();

    int actualSamples = (int)(numBytes / PICO8_BLOCK_SIZE);
    D(printf("got data, have bytes %d, actual samples %d\n",
        numBytes, actualSamples));
    // assert(actualSamples <= numSamples);
    if (actualSamples > numSamples) {
        I(printf("got too much data, have bytes %d, "
            "actual samples %d, wanted samples %d, clipping\n",
            numBytes, actualSamples, numSamples));
        actualSamples = numSamples;
    }
    // refresh the actual samples acquired
    setIntegerParam(mPico8ActualSamples, actualSamples);

    // allocate NDArray for enabled channels
    epicsFloat32 *pDatas[PICO8_NR_CHANNELS] = {0};
    for (int a = 0; a < PICO8_NR_CHANNELS; a++) {
        int chEnable = 0;
        getIntegerParam(a, mPico8CHEnable, &chEnable);
        if (chEnable) {
            D(printf("channel enabled, addr %d\n", a));
            size_t dims[1];
            dims[0] = actualSamples;
            this->pArrays[a] = pNDArrayPool->alloc(1, dims, NDFloat32, 0, 0);
            pDatas[a] = (epicsFloat32 *)this->pArrays[a]->pData;
        } else {
            D(printf("channel disabled, addr %d\n", a));
        }
    }

    // copy the data from the device raw buffer into enabled channel
    // NDarray
    epicsFloat32 *pRawData = (epicsFloat32 *)(mRawDataBuf);
    for (int i = 0; i < actualSamples; i++) {
        // de-interleave the data
        for (int a = 0; a < PICO8_NR_CHANNELS; a++) {
            // only if channel is enabled
            if (this->pArrays[a]) {
                epicsFloat32 *pData = pDatas[a];
                assert(pData != NULL);
                *(pData + i) = *pRawData;
            }
            pRawData++;
        }
    }

    return 0;
}

void pico8::dataTask(void) {
    int status = asynSuccess;
    int arrayCounter;
    int ret;
    epicsTimeStamp startTime, endTime;
    static const char *functionName = "dataTask";

    sleep(1);

    this->lock();

    if (! mRunThread) {
        E(printf("data thread will not start!\n"));
        this->unlock();
        return;
    }

    I(printf("data thread started\n"));

    /* Loop forever */
    while (mRunThread) {
        /* Has acquisition been stopped? */
        status = epicsEventTryWait(this->stopEventId_);
        if (status == epicsEventWaitOK) {
            acquiring_ = 0;
        }

        /* If we are not acquiring then wait for a semaphore that is
           given when acquisition is started */
        if (!acquiring_) {
            // XXX not using abortRead() since it make next read
            //     fail and then we are in endless loop
            //     of failing to read. see kernel driver..
            //     OTOH, not sure if needed in the first place?!
            // I(printf("aborting read..\n"));
            // abortRead();

            setIntegerParam(mPico8Acquire, 0);

            /* Call the callbacks to update any changes */
            callParamCallbacks();

            /* Release the lock while we wait for an event that says
               acquire has started, then lock again */
            asynPrint(this->pasynUserSelf, ASYN_TRACE_FLOW,
                "%s:%s: waiting for acquire to start\n", driverName, functionName);
            I(printf("waiting for acquire to start..\n"));
            // INF_MSG("IDLE");
            this->unlock();
            status = epicsEventWait(this->startEventId_);
            this->lock();
            if (! mRunThread) {
                I(printf("data thread was stopped!\n"));
                break;
            }
            acquiring_ = 1;
            I(printf("acquire started\n"));
        }
        epicsTimeGetCurrent(&startTime);

        INF_MSG("ACQUIRING");

        /* Call the callbacks to update any changes */
        callParamCallbacks();

        // get the data from the device and fill individual
        // enabled channel with data
        // if no channels are enabled we stop acquiring!
        ret = acquireArrays();
        if (ret == -1) {
            E(printf("stopping acquisition!!\n"));
            acquiring_ = 0;
        }

        if (! acquiring_) {
            continue;
        }

        INF_MSG("ACQUIRE DONE");

        /* Call the callbacks to update any changes */
        callParamCallbacks();

        epicsTimeStamp frameTime;
        epicsTimeGetCurrent(&frameTime);
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        uniqueId_++;
        D(printf("acquisition %d done..\n", arrayCounter));

        // notify clients of the new data arrays for enabled
        // channels
        for (int a = 0; a < PICO8_NR_CHANNELS; a++) {
            // call the callbacks to update any changes (on all channels,
            // enabled or disabled)
            callParamCallbacks(a);

            // call the array callbacks to update only enabled channels
            if (this->pArrays[a]) {
                NDArray *pData = this->pArrays[a];

                // set the frame number and time stamp
                pData->uniqueId = uniqueId_;
                pData->timeStamp = frameTime.secPastEpoch + frameTime.nsec / 1.e9;
                updateTimeStamp(&pData->epicsTS);

                // get any attributes that have been defined for this driver
                this->getAttributes(pData->pAttributeList);

                // must release the lock here, or we can get into a deadlock, because we can
                // block on the plugin lock, and the plugin can be calling us
                this->unlock();
                D(printf("doCallbacksGenericPointer for pArray %d..\n", a));
                doCallbacksGenericPointer(pData, NDArrayData, a);
                this->lock();
            } else {
                D(printf("NULL pArray %d..\n", a));
            }
        }

        /* Call the callbacks to update any changes */
        callParamCallbacks();

        // measure time elapsed from waiting for gate high to gate high
        epicsTimeGetCurrent(&endTime);
// #if _DBG == 1
        // double elapsedTime = epicsTimeDiffInSeconds(&endTime, &startTime);
        D(printf("acq took %2.3f s\n", epicsTimeDiffInSeconds(&endTime, &startTime)));
// #endif
        // XXX: this is a hack to throttle the acquisition
        //      if gate is set to always high (gateMux == 0)
        int gateMux;
        getIntegerParam(mPico8GateMux, &gateMux);
        if (gateMux == 0) {
            D(printf("rate limit: sleeping ..\n"));
            this->unlock();
            int rateLimit;
            getIntegerParam(mPico8RateLimit, &rateLimit);
            sleep(rateLimit);
            this->lock();
            D(printf("rate limit: .. awake!\n"));
            callParamCallbacks();
        }
    }

    this->unlock();
    callParamCallbacks();
    I(printf("data thread is down!\n"));
}

asynStatus pico8::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    const char *name;
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    int addr = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    D(printf("handle %d:%d %s = %d\n", addr, function, name, value));

    /* Set the parameter and readback in the parameter library.
       This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setIntegerParam(addr, function, value);

    if (function == mPico8Acquire) {
        setAcquire(value);
    } else if (function == mPico8CHRange) {
        D(printf("OLD range bitmask %2X\n", mChRangeMask));
        if (value) {
            mChRangeMask |= (1 << addr);
        } else {
            mChRangeMask &= ~(1 << addr);
        }
        D(printf("NEW range bitmask %2X\n", mChRangeMask));
        this->setRange(mChRangeMask);
    } else if (function == mPico8FSamp) {
        this->setFSamp(value);
    } else if (function == mPico8TrgMode
            || function == mPico8TrgCh
            || function == mPico8TrgLength
            || function == mPico8TrgLevelUnits) {
        this->setTrigger();
    } else if (function == mPico8RingBuf) {
        if (value > 1023) {
            value = 1023;
        }
        if (value < 0) {
            value = 0;
        }
        this->setRingBuf(value);
    } else if (function == mPico8GateMux) {
        this->setGateMux(value);
    } else if (function == mPico8ConvMux) {
        this->setConvMux(value);
    } else if (function < PICO8_FIRST_PARAM) {
        /* If this parameter belongs to a base class call its method */
        status = asynNDArrayDriver::writeInt32(pasynUser, value);
    }

    if ((function == mPico8FSamp) || (function == mPico8NumSamples)) {
        int numSamples;
        int samplingFreq;
        getIntegerParam(mPico8NumSamples, &numSamples);
        getIntegerParam(mPico8FSamp, &samplingFreq);
        D(printf("using number of samples %d\n", numSamples));
        D(printf("using sampling frequency %d Hz\n", samplingFreq));

        // default to 1; equals sample count
        double tick = 1.0;
        // set the right tick according to sampling frequency, if valid
        if (samplingFreq > 0 && samplingFreq <= 1e6) {
            tick = 1.0 / (epicsFloat64) samplingFreq;
        }
        setDoubleParam(mPico8TickValue, tick);
        D(printf("New timeX tick set to %g s\n", tick));
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks(addr);

    if (status) {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeInt32 error, status=%d function=%d, value=%d\n",
              driverName, status, function, value);
    } else {
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeInt32 function=%d, value=%d\n",
              driverName, function, value);
    }

    return status;
}

asynStatus pico8::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    const char *name;
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    int addr = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    D(printf("handle %d:%d %s = %f\n", addr, function, name, value));

    /* Set the parameter and readback in the parameter library.
       This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setDoubleParam(addr, function, value);

    if (function == mPico8TrgLevel) {
        this->setTrigger();
    } else if (function < PICO8_FIRST_PARAM) {
        /* If this parameter belongs to a base class call its method */
        status = asynNDArrayDriver::writeFloat64(pasynUser, value);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks(addr);

    if (status) {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeFloat64 error, status=%d function=%d, value=%f\n",
              driverName, status, function, value);
    } else {
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeFloat64 function=%d, value=%f\n",
              driverName, function, value);
    }

    return status;
}

/** Report status of the driver.
 * Prints details about the detector in us if details>0.
 * It then calls the ADDriver::report() method.
 * \param[in] fp File pointed passed by caller where the output is written to.
 * \param[in] details Controls the level of detail in the report. */
void pico8::report(FILE *fp, int details) {
//    static const char *functionName = "report";

    fprintf(fp, "CAENels AMC-Pico-8 port=%s\n", this->portName);
    if (details > 0) {
        /* XXX: Add some details? */
    }
    // Call the base class method
    asynNDArrayDriver::report(fp, details);
}

/** Configuration command, called directly or from iocsh */
extern "C" int pico8Configure(const char *portName, const char *devicePath, int numSamples,
                                int maxBuffers, size_t maxMemory, int priority, int stackSize)
{
    new pico8(portName, devicePath, numSamples,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */
static const iocshArg arg0 = { "portName",   iocshArgString};
static const iocshArg arg1 = { "devicePath", iocshArgString};
static const iocshArg arg2 = { "numSamples", iocshArgInt};
static const iocshArg arg3 = { "maxBuffers", iocshArgInt};
static const iocshArg arg4 = { "maxMemory",  iocshArgInt};
static const iocshArg arg5 = { "priority",   iocshArgInt};
static const iocshArg arg6 = { "stackSize",  iocshArgInt};
static const iocshArg * const configArgs[] = {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6};
static const iocshFuncDef config = {"pico8Configure", 7, configArgs};
static void configCallFunc(const iocshArgBuf *args)
{
    pico8Configure(args[0].sval, args[1].sval, args[2].ival,
            args[3].ival, args[4].ival, args[5].ival, args[6].ival);
}

extern "C" void pico8Register(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
epicsExportRegistrar(pico8Register);
}
