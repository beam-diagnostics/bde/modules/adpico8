/*
 * pico8.h
 *
 *  Created on: Apr 29, 2016
 *      Author: hinkokocevar
 */

#ifndef _PICO8_H_
#define _PICO8_H_

#include <epicsTypes.h>
#include <epicsEvent.h>
#include <epicsTime.h>
#include <asynNDArrayDriver.h>

#define MAX_LOG_STR_LEN                256
#define MSG(p, s) ({\
    snprintf(mLogStr, MAX_LOG_STR_LEN, "%s %s", \
                p, s); \
    asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, \
              "%s::%s : %s\n", driverName, __func__, mLogStr); \
    setStringParam(mPico8Message, mLogStr); \
})
#define INF_MSG(s)      ({ MSG("[INF]", s); })
#define ERR_MSG(s)      ({ MSG("[ERR]", s); })

// #define _DBG 1
#if _DBG == 1
    #undef D0
    #define D0(x) { printf("\n"); }
    #undef D
    #define D(x) { \
    printf("[DBG] %s::%s: ", driverName, __func__); \
    x; \
}
#else
    #undef D0
    #define D0(x)
    #undef D
    #define D(x)
#endif
#define E(x) { \
    printf("[ERR] %s::%s: ", driverName, __func__); \
    x; \
}
#define I(x) { \
    printf("[INF] %s::%s: ", driverName, __func__); \
    x; \
}

/* Maximum number of channels is fixed */
#define PICO8_NR_CHANNELS            8
/* Sample block size, 8x 32-bits (IEEE 754 float) = 32 bytes */
#define PICO8_BLOCK_SIZE             (PICO8_NR_CHANNELS * sizeof(epicsFloat32))
/* For strings */
#define PICO8_BUFFER_SIZE            256

// digitizer
#define Pico8Acquire_            "PICO8.ACQUIRE"          /* (asynParamInt32, r/w) acquire control */
#define Pico8NumSamples_         "PICO8.NUM_SAMPLES"      /* (asynParamInt32, r/w) number of samples */
#define Pico8FSamp_              "PICO8.FSAMP"            /* (asynParamInt32, r/w) sampling frequency */
#define Pico8TickValue_          "PICO8.TICK_VALUE"       /* (asynParamFloat64, r/o) tick delta */
#define Pico8ActualSamples_      "PICO8.ACTUAL_SAMPLES"   /* (asynParamInt32, r/o) no. of transferred samples */
#define Pico8TrgMode_            "PICO8.TRGMODE"          /* (asynParamInt32, w/o) trigger mode */
#define Pico8TrgCh_              "PICO8.TRGCH"            /* (asynParamInt32, w/o) trigger channel */
#define Pico8TrgLevel_           "PICO8.TRGLEVEL"         /* (asynParamFloat64, r/w) trigger level */
#define Pico8TrgLength_          "PICO8.TRGLENGTH"        /* (asynParamInt32, w/o) trigger length */
#define Pico8RingBuf_            "PICO8.RINGBUF"          /* (asynParamInt32, w/o) pre-trigger sample count */
#define Pico8GateMux_            "PICO8.GATEMUX"          /* (asynParamInt32, w/o) gate source */
#define Pico8ConvMux_            "PICO8.CONVMUX"          /* (asynParamInt32, w/o) start of conversion source */
#define Pico8Message_            "PICO8.MESSAGE"          /* (asynOctetRead, r/o) driver message */
#define Pico8RateLimit_          "PICO8.RATELIMIT"        /* (asynParamInt32, r/w) rate limit with continuos trigger */
#define Pico8TrgLevelUnits_      "PICO8.TRGLEVEL_UNITS"   /* (asynParamInt32, r/w) trigger level units */

// individual channel
#define Pico8CHEnable_           "PICO8.CH.ENABLE"        /* (asynParamInt32, r/w) channel enable */
#define Pico8CHRange_            "PICO8.CH.RANGE"         /* (asynParamInt32, r/w) channel range */

/** CAENels AMC-Pico-8 driver */
class epicsShareClass pico8 : public asynNDArrayDriver {
public:
    pico8(const char *portName, const char *devicePath, int numSamples,
        int maxBuffers, size_t maxMemory, int priority, int stackSize);
    ~pico8();

    /* These methods override the virtual methods in the base class */
    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual void report(FILE *fp, int details);

    /**< Should be private, but gets called from C, so must be public */
    void dataTask(void);

private:
    /* These are the methods that are new to this class */
    int acquireArrays();
    void setAcquire(int value);
    int openDevice();
    void closeDevice();
    int readDevice(void *buf, int samp, int *count);
    int abortRead();
    int setFSamp(uint32_t val);
    int setRange(uint8_t val);
    int getBTrans(int *val);
    int setGateMux(uint32_t val);
    int setConvMux(uint32_t val);
    int setRingBuf(uint32_t val);
    int setTrigger();

protected:
    // digitizer
    int mPico8Acquire;
#define PICO8_FIRST_PARAM mPico8Acquire
    int mPico8NumSamples;
    int mPico8FSamp;
    int mPico8TickValue;
    int mPico8ActualSamples;
    int mPico8TrgMode;
    int mPico8TrgCh;
    int mPico8TrgLevel;
    int mPico8TrgLength;
    int mPico8RingBuf;
    int mPico8GateMux;
    int mPico8ConvMux;
    int mPico8Message;
    int mPico8RateLimit;
    int mPico8TrgLevelUnits;
    // individual channel
    int mPico8CHEnable;
    int mPico8CHRange;

private:
    int mHandle;
    void *mRawDataBuf;
    int mRawDataBufSize;
    char mDevicePath[PICO8_BUFFER_SIZE];
    char mManufacturerName[PICO8_BUFFER_SIZE];
    char mDeviceName[PICO8_BUFFER_SIZE];
    char mSerialNumber[PICO8_BUFFER_SIZE];
    char mFirmwareRevision[PICO8_BUFFER_SIZE];
    uint8_t mChRangeMask;
    bool mRunThread;
    char mLogStr[MAX_LOG_STR_LEN];

    epicsEventId startEventId_;
    epicsEventId stopEventId_;
    int uniqueId_;
    int acquiring_;
};

#endif /* _PICO8_H_ */
